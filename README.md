# dpp
DPP - DBot Prop Protection

Gmod addon

Licensed under Apache License v2.0

http://www.apache.org/licenses/LICENSE-2.0

https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)

More info at workshop: http://steamcommunity.com/sharedfiles/filedetails/?id=659044893

If you want to use MySQL, locate dpp_config_example.lua in the addon lua root folder, rename it to dpp_config.lua and then open and edit it