
--[[
Copyright (C) 2016-2017 DBot

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
]]

E2Helper.Descriptions.getOwner = 'Gets DPP owner of entity'
E2Helper.Descriptions.setOwner = 'Sets DPP owner of entity to a player (if you are an owner).\nReturns 1 if successful, 0 otherwise'
E2Helper.Descriptions.isGhosted = 'Is entity is ghosted'
E2Helper.Descriptions.isOwnedByLocalPlayer = 'Is entity owned by Expression 2 owner'
E2Helper.Descriptions.getOwnedProps = 'Returns array of owned props'
E2Helper.Descriptions.isSingleOwner = 'Is entity owned by one player'
E2Helper.Descriptions.isModelBlocked = 'Is model blacklisted to Expression 2 Owner'

E2Helper.Descriptions.dppCanDrive = 'Can local player drive entity (property menu -> Drive)'
E2Helper.Descriptions.dppCanUse = 'Can local player use entity'
E2Helper.Descriptions.dppCanProperty = 'Can local player use property on entity'
E2Helper.Descriptions.dppCanEditVariable = 'Can local player edit entity vatiable'
E2Helper.Descriptions.dppCanEnterVehicle = 'Can local player enter vehicle'
E2Helper.Descriptions.dppCanTool = 'Can local player use tool on entity'
E2Helper.Descriptions.dppCanGravgunPunt = 'Can local player punt using gravgun entity'
E2Helper.Descriptions.dppCanGravgun = 'Can local player gravgun entity'
E2Helper.Descriptions.dppCanDamage = 'Can local player damage entity'
